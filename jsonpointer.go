package propeller

import (
	"fmt"
	"strconv"
	"strings"
)

type JSONPointerError struct {
	What string
}

func (e *JSONPointerError) Error() string {
	return "JSON Pointer error: " + e.What
}

// Escape takes a string and returns a variant of it suitable for use as part of
// a JSON Pointer.
func Escape(pointer string) string {
	pointer = strings.Replace(pointer, "~", "~0", -1)
	pointer = strings.Replace(pointer, "/", "~1", -1)
	return pointer
}

func Get(j *interface{}, pointer string) (*interface{}, error) {
	if j == nil {
		return nil, nil
	}

	if pointer == "" {
		return j, nil
	}

	parts := strings.SplitN(pointer, "/", 3)
	if parts[0] != "" {
		return nil, &JSONPointerError{fmt.Sprintf("Pointer '%s' doesn't begin with '/'", pointer)}
	}

	head := Unescape(parts[1])

	var tail string
	if len(parts) > 2 {
		tail = "/" + parts[2]
	} else {
		tail = ""
	}

	e := *j
	switch e.(type) {
	case []interface{}:
		// Array
		index, err := strconv.ParseInt(head, 10, 64)
		if err != nil {
			return nil, err
		}
		a := e.([]interface{})
		if index >= int64(len(a)) {
			return nil, &JSONPointerError{"No such index"}
		}
		return Get(&a[index], tail)

	case map[string]interface{}:
		// Object
		m := e.(map[string]interface{})
		sub, ok := m[head]
		if !ok {
			return nil, &JSONPointerError{"No such name"}
		}
		return Get(&sub, tail)

	default:
		// FIXME Assert tail is empty
		// return j, nil
	}

	return nil, &JSONPointerError{"Bad type"}
}

func Unescape(pointer string) string {
	// This is performed by first transforming any occurrence of the sequence
	// '~1' to '/', and then transforming any occurrence of the sequence '~0' to
	// '~'.
	pointer = strings.Replace(pointer, "~1", "/", -1)
	pointer = strings.Replace(pointer, "~0", "~", -1)
	return pointer
}
