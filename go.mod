// -*- Text -*-

module gitlab.com/propellerproject/libpropeller-golang

require (
	github.com/shurcooL/httpfs v0.0.0-20171119174359-809beceb2371 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20180915214035-33ae1944be3f
	golang.org/x/tools v0.0.0-20181008205924-a2b3f7f249e9 // indirect
)
