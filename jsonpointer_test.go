package propeller

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestGet(t *testing.T) {
	b := []byte(`
{
  "foo": ["bar", "baz"],
  "": 0,
  "a/b": 1,
  "c%d": 2,
  "e^f": 3,
  "g|h": 4,
  "i\\j": 5,
  "k\"l": 6,
  " ": 7,
  "m~n": 8
}
`)
	var j interface{}
	err := json.Unmarshal(b, &j)
	if err != nil {
		t.FailNow()
	}

	cases := []struct {
		in   string
		want interface{}
	}{
		{"", j},
		{"/foo", []interface{}{"bar", "baz"}},
		{"/foo/0", "bar"},
		{"/", 0.},
		{"/a~1b", 1.},
		{"/c%d", 2.},
		{"/e^f", 3.},
		{"/g|h", 4.},
		{"/i\\j", 5.},
		{"/k\"l", 6.},
		{"/ ", 7.},
		{"/m~0n", 8.},
	}

	for _, c := range cases {
		got, err := Get(&j, c.in)
		if err != nil {
			t.Errorf("Get(%q) failed: %s", c.in, err.Error())
		} else {
			if !reflect.DeepEqual(*got, c.want) {
				t.Errorf("Get(%q) == %d, want %d", c.in, *got, c.want)
			}
		}
	}
}
