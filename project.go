//go:generate go run -tags=dev predef_generate.go

package propeller

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"

	// For go generate
	_ "github.com/shurcooL/vfsgen"
)

type Project struct {
	Dir  string
	data interface{}
}

func (p *Project) BuildOrder(command string) ([]string, error) {
	deps, err := p.dependencies(command, []string{})
	if err != nil {
		return nil, err
	}
	return append(deps, command), nil
}

func (p *Project) Data() (*interface{}, error) {
	if err := p.load(); err != nil {
		return nil, err
	}
	return &p.data, nil
}

func (p *Project) Expand(s string) string {
	r := regexp.MustCompile(`(\$+)\{([^}]*)\}`)
	e := r.ReplaceAllStringFunc(s, func(m string) string {
		groups := r.FindStringSubmatch(m)
		dollars := len(groups[1])
		if dollars%2 == 0 {
			return groups[1][0:dollars/2] + "{" + groups[2] + "}"
		}

		repl, _ := p.Get(groups[2])
		var replStr string
		if repl != nil {
			replStr, _ = (*repl).(string)
		}
		return groups[1][0:dollars/2] + replStr
	})
	return e
}

func (p *Project) Get(path string) (*interface{}, error) {
	if err := p.load(); err != nil {
		return nil, err
	}
	val, err := Get(&p.data, path)
	if err != nil {
		return val, err
	}
	switch (*val).(type) {
	case string:
		*val = p.Expand((*val).(string))
	}
	return val, nil
}

func (p *Project) Run(commandName string) error {
	commandData, err := p.Get("/commands/" + commandName)
	if err != nil {
		return &ProjectError{"No such command"}
	}

	_, ok := (*commandData).(map[string]interface{})
	if !ok {
		return &ProjectError{"Command data is not an object"}
	}

	commands, err := p.BuildOrder(commandName)
	if err != nil {
		log.Fatalln("Failed to compute build order")
	}
	for _, command := range commands {
		cmd, err := p.Get("/commands/" + commandName + "/command")
		if cmd == nil || err != nil {
			log.Fatalln("Command has no command")
		}
		cmdStr, ok := (*cmd).(string)
		if !ok {
			log.Fatalln("Command's command not a string")
		}

		dir := p.dirForCommand(command)

		err = runOne(cmdStr, dir)
		if err != nil {
			log.Fatalln("Failed running command")
		}
	}

	return nil
}

func (p *Project) dependencies(command string, trace []string) ([]string, error) {
	if contains(trace, command) {
		return nil, &ProjectError{"Command eventually depends on itself"}
	}

	el, err := p.Get("/commands/" + command + "/depends")
	if el == nil || err != nil {
		return []string{}, nil
	}

	deps, ok := (*el).([]interface{})
	// Not an array
	if !ok {
		return nil, &ProjectError{"Command’s `depends` must be an array"}
	}

	trace = append(trace, command)
	var indirect []string
	var direct []string
	for _, dep := range deps {
		str, ok := dep.(string)
		// Not a string
		if !ok {
			return nil, &ProjectError{"Command’s dependencies must be strings"}
		}

		if str == command {
			return nil, &ProjectError{"Command immediately depends on itself"}
		}

		direct = append(direct, str)

		tmp, err := p.dependencies(str, trace)
		if err != nil {
			return nil, err
		}
		for _, el := range tmp {
			if !contains(direct, el) &&
				!contains(indirect, el) {
				indirect = append(indirect, el)
			}
		}
	}

	return append(indirect, direct...), nil
}

func (p *Project) dirForCommand(cmd string) string {
	if commandDir, err := p.Get("/commands/" + cmd + "/dir"); commandDir != nil && err == nil {
		if commandDirStr, ok := (*commandDir).(string); ok {
			return commandDirStr
		}

		log.Fatalln("Command's dir is not a string")
	}

	return p.Dir
}

func (p *Project) load() error {
	if p.data == nil {

		data, err := fileToJSON(path.Join(p.Dir, "project.json"))
		if err != nil {
			return err
		}

		userJSON := path.Join(p.Dir, "project.user.json")
		if fileExists(userJSON) {
			var userData interface{}
			userData, err = fileToJSON(userJSON)
			if err != nil {
				return err
			}
			data = merge(userData, data)
		}

		if typee, err := Get(&data, "/type"); err == nil {
			if typeName, ok := (*typee).(string); ok {
				predefData, err := PredefData(typeName)
				if err != nil {
					return err
				}

				data = merge(data, predefData)
			} else {
				log.Fatalln("Type not a string")
			}
		}

		if err := p.normalise(&data); err != nil {
			log.Fatalln("Failed to normalise data:", err)
		}

		p.data = data
	}

	return nil
}

func (p *Project) normalise(j *interface{}) error {
	obj, ok := (*j).(map[string]interface{})
	if !ok {
		return &ProjectError{"Can't normalise non-object"}
	}

	obj["dir"] = p.Dir

	if _, ok := obj["name"]; !ok {
		obj["name"] = path.Base(p.Dir)
	}

	if commands, ok := obj["commands"]; ok {
		commands := commands.(map[string]interface{})
		for k, v := range commands {
			switch v.(type) {
			case string:
				commands[k] = map[string]interface{}{"command": v}

			case map[string]interface{}:
				command := v.(map[string]interface{})
				depends := command["depends"]
				switch depends.(type) {
				case string:
					command["depends"] = []string{depends.(string)}
					commands[k] = command

				default:
					return &ProjectError{fmt.Sprintf("Command's dependencies neither string nor array: %T", depends)}

				case []interface{}:
				case nil:
				}
			}
		}
	}

	*j = obj

	return nil
}

type ProjectError struct {
	What string
}

func (e *ProjectError) Error() string {
	return e.What
}

func ProjectDirFor(file string) (string, error) {
	abs, err := filepath.Abs(file)
	if err != nil {
		return "", err
	}

	if fileExists(path.Join(abs, "project.json")) {
		return abs, nil
	}

	parent := path.Dir(abs)
	if parent == abs {
		return "", &ProjectError{"Couldn't find project file"}
	}

	return ProjectDirFor(parent)
}

func ProjectFor(file string) (*Project, error) {
	projectDir, err := ProjectDirFor(file)
	if err != nil {
		return nil, err
	}

	return &Project{Dir: projectDir}, nil
}

func contains(haystack []string, needle string) bool {
	for _, el := range haystack {
		if el == needle {
			return true
		}
	}
	return false
}

func fileExists(file string) bool {
	_, err := os.Stat(file)
	return (!os.IsNotExist(err))
}

func fileToJSON(filePath string) (interface{}, error) {
	jsonFile, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	var data interface{}
	jsonParser := json.NewDecoder(jsonFile)
	if err = jsonParser.Decode(&data); err != nil {
		return nil, err
	}
	return data, nil
}

// func isString(e interface{}) bool {
// 	_, ok := e.(string)
// 	return ok
// }

func merge(pri interface{}, sec interface{}) interface{} {
	switch pri.(type) {
	case map[string]interface{}:
		// Object
		if secObj, ok := sec.(map[string]interface{}); ok {
			priObj := pri.(map[string]interface{})
			for key, val := range secObj {
				priObj[key] = merge(priObj[key], secObj[key])
				if _, ok := priObj[key]; ok {
				} else {
					priObj[key] = val
				}
			}
			return priObj
		}
		return pri

	default:
		if pri == nil {
			return sec
		}
		return pri
	}
}

func PredefData(typeName string) (interface{}, error) {
	data, err := predef.Open(typeName + ".json")
	if data == nil || err != nil {
		return nil, &ProjectError{"Unknown project type"}
	}

	var j interface{}
	err = json.NewDecoder(data).Decode(&j)
	if err != nil {
		return nil, &ProjectError{"Failed to parse predefined type data (this *really* shouldn't happen): " + err.Error()}
	}

	return j, nil
}

func runOne(cmd string, dir string) error {
	if err := os.MkdirAll(dir, 0777); err != nil {
		return err
	}

	if err := os.Chdir(dir); err != nil {
		return err
	}

	c := exec.Command("/bin/sh", "-x", "-c", cmd)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	return c.Run()
}
