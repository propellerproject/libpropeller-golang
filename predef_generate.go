// +build ignore

package main

import (
	"log"
	"net/http"

	"github.com/shurcooL/vfsgen"
)

func main() {
	if err := vfsgen.Generate(http.Dir("predef/data"), vfsgen.Options{
		PackageName:  "propeller",
		VariableName: "predef",
	}); err != nil {
		log.Fatalln(err)
	}
}
