package propeller

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestBuildOrder(t *testing.T) {
	p, err := projectFromJSON(`{
  "commands": {
    "bad":   { "depends": [ "bad" ] },
    "bad2":  { "depends": [ "bad3" ] },
    "bad3":  { "depends": [ "bad2" ] },
    "one":   {},
    "two":   { "depends": [ "one" ] },
    "three": { "depends": [ "one" ] },
    "four":  { "depends": [ "one", "two" ] },
    "five":  { "depends": [ "two", "three" ] }
  }
}`)

	if err != nil {
		t.Errorf("Project JSON error: %s", err)
	}

	cases := []struct {
		Command  string
		Expected []string
	}{
		{"bad", nil},
		{"bad2", nil},
		{"bad3", nil},
		{"one", []string{"one"}},
		{"two", []string{"one", "two"}},
		{"three", []string{"one", "three"}},
		{"four", []string{"one", "two", "four"}},
		{"five", []string{"one", "two", "three", "five"}},
	}

	for _, c := range cases {
		actual, err := p.BuildOrder(c.Command)
		if c.Expected != nil && err != nil {
			t.Errorf("`BuildOrder()` error: %s", err)
		} else if !reflect.DeepEqual(actual, c.Expected) {
			t.Errorf(
				"\"%s\" -> %q (actual) != %q (expected)",
				c.Command,
				actual,
				c.Expected,
			)
		}
	}
}

func TestExpand(t *testing.T) {
	p := Project{"", map[string]interface{}{
		"bar": "baz",
		"baz": "foo$${/bar}foo",
		"foo": "foo${/bar}foo",
	}}

	cases := []struct {
		Pointer  string
		Expected string
	}{
		{"/foo", "foobazfoo"},
		{"/baz", "foo${/bar}foo"},
	}

	for _, c := range cases {
		act, err := p.Get(c.Pointer)
		if err != nil {
			t.Errorf("Error: %s", err)
		}
		if (*act).(string) != c.Expected {
			t.Errorf("%q != %q", *act, c.Expected)
		}
	}
}

func TestMerge(t *testing.T) {
	cases := []struct {
		Pri map[string]interface{}
		Sec map[string]interface{}
		Exp map[string]interface{}
	}{

		// Two empties
		{
			map[string]interface{}{},
			map[string]interface{}{},
			map[string]interface{}{},
		},

		// Scalar value doesn't overwrite scalar value
		{
			map[string]interface{}{"foo": "bar"},
			map[string]interface{}{"foo": "baz"},
			map[string]interface{}{"foo": "bar"},
		},

		// Scalar value overwrites missing
		{
			map[string]interface{}{},
			map[string]interface{}{"foo": "baz"},
			map[string]interface{}{"foo": "baz"},
		},

		// Scalar value doesn't overwrite object value
		{
			map[string]interface{}{
				"foo": map[string]interface{}{
					"bar": "baz",
				},
			},
			map[string]interface{}{"foo": "bar"},
			map[string]interface{}{
				"foo": map[string]interface{}{
					"bar": "baz",
				},
			},
		},

		// Deep merge
		{
			map[string]interface{}{
				"foo": map[string]interface{}{
					"bar": map[string]interface{}{
						"baz": "qux",
					},
				},
			},
			map[string]interface{}{
				"foo": map[string]interface{}{
					"bar": map[string]interface{}{
						"new": "value",
					},
				},
			},
			map[string]interface{}{
				"foo": map[string]interface{}{
					"bar": map[string]interface{}{
						"baz": "qux",
						"new": "value",
					},
				},
			},
		},
	}

	for _, c := range cases {
		act := merge(c.Pri, c.Sec)
		if !reflect.DeepEqual(act, c.Exp) {
			t.Errorf("merge(%q, %q) == %q, want %q",
				c.Pri,
				c.Sec,
				act,
				c.Exp)
		}
	}
}

func TestPredefData(t *testing.T) {
	data, err := PredefData("make")
	if err != nil {
		t.Errorf("Error: %s", err)
	}

	commands, ok := data.(map[string]interface{})["commands"]

	if ok {
		if len(commands.(map[string]interface{})) < 10 {
			t.Errorf("Bad data; too few commands")
		}
	} else {
		t.Errorf("Bad data")
	}

	_, err = PredefData("gobbledygook")
	if err == nil {
		t.Errorf("Should have failed on non-existing type")
	}
}

func projectFromJSON(j string) (*Project, error) {
	var i interface{}
	err := json.Unmarshal([]byte(j), &i)
	if err != nil {
		return nil, err
	}
	return &Project{"", i}, nil
}
